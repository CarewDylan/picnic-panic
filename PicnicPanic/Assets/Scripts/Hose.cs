﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hose : MonoBehaviour {
	bool followTouch = false;
	float rainDropSpawnRate = 0.2f;
	float timeSinceLastSpawned = 0;
	public Rain rainDrop; 
	Vector3 currentPos;
	int maxNumberRainDrop = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		timeSinceLastSpawned += Time.deltaTime;
		if (followTouch) {
			for (int i = 0; i < Input.touchCount; i++) {
				Vector3 posFromScreen = Camera.main.ScreenToWorldPoint (Input.GetTouch (i).position); 
				currentPos = new Vector3 (posFromScreen.x, posFromScreen.y, 10);
				gameObject.transform.position = currentPos; 
				spawnRainDrop ();
			}
		} else {
			
			timeSinceLastSpawned = 0;
		}
	}

	public void enableHose(bool enable) {
		if (enable) {
			gameObject.SetActive (true);
			followTouch = true; 
			maxNumberRainDrop = 0;
		} else {
			gameObject.SetActive (false); 
			followTouch = false;
			maxNumberRainDrop = 0;
		}
	}

	public void spawnRainDrop() {
		if (timeSinceLastSpawned >= rainDropSpawnRate) {
			if (maxNumberRainDrop <= 9) {
				Instantiate (rainDrop, currentPos, Quaternion.identity);
				maxNumberRainDrop++;
			}

			timeSinceLastSpawned = 0;
		}
	}
}
