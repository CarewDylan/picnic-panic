﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TouchControl : MonoBehaviour {

	RaycastHit2D hit;
	public bool waterDamage = false; 
	//public FireAnt ant; 
	public Hose hose; 
	public int score; 
	public static TouchControl controlManager; 
	public Text scoreKeeper;
	public Text gameOverText; 
	public bool gameOver = false;
	public GameObject scoreGameOver; 
	public int gameOverTapCount;

	//Button
	public Button waterButton;
	public Sprite buttonOff; 
	public Sprite buttonOn;
	public float timeUntilPowerUp;

	// Use this for initialization
	void Start () {
		controlManager = this;
		GameOverSetup ();
		//waterDamage = true;
	}
	
	// Update is called once per frame
	void Update () {
		timeUntilPowerUp += Time.deltaTime;
		if (gameOver) {
			GameOverSetup ();
		}

		if (timeUntilPowerUp >= 20.0f) {
			waterButton.GetComponent<Image> ().sprite = buttonOn;
		} 
		scoreKeeper.text = score.ToString ();
		if (Input.touchCount > 0) {
			for (int i = 0; i < Input.touchCount; i++) {
				hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.GetTouch(i).position), Vector2.zero);

				if (hit.collider && !waterDamage && (Input.GetTouch(i).phase == TouchPhase.Began)) {
					if (hit.collider.tag == "FireAnt") {
						FireAnt ant = (FireAnt)(((GameObject)hit.collider.gameObject).GetComponent<FireAnt> ());
						ant.decreaseHealth ();
					} 
					if (hit.collider.tag == "AntBoss") {
						AntBoss ant = (AntBoss)(((GameObject)hit.collider.gameObject).GetComponent<AntBoss> ());
						ant.DecreaseHeatlh ();
					}
					//CALL to decrease enemy health; 
				}

				if (Input.GetTouch(i).phase == TouchPhase.Moved && waterDamage) {
					hose.enableHose (waterDamage);
				}
			}
		}
	}

	public void waterDamager() {

		if (timeUntilPowerUp >= 20.0f) {
			waterDamage = true;
		}
		if (waterDamage) {
			waterButton.GetComponent<Image> ().sprite = buttonOn;
			hose.enableHose (waterDamage);
			StartCoroutine (StopWaterDamage ());
			timeUntilPowerUp = 0;
		}  
	}

	public void GameOverSetup () {
		if (gameOver) {
			gameOverText.gameObject.SetActive (true);
			scoreKeeper.gameObject.SetActive (false);
			scoreGameOver.gameObject.SetActive (true);
			Component[] score = (scoreGameOver.GetComponentsInChildren<Text> ());
			((Text)(score [0])).text = scoreKeeper.text;


			if (Input.touchCount > 0) {
				for (int i = 0; i < Input.touchCount; i++) {
					if (Input.GetTouch(i).phase == TouchPhase.Began) {
						gameOverTapCount++;
					}
				}

				if (gameOverTapCount >= 3) {
					SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
				}
			}
		} else {
			gameOverText.gameObject.SetActive (false);
			scoreKeeper.gameObject.SetActive (true);
			scoreGameOver.gameObject.SetActive (false);
		}
	}

	public IEnumerator StopWaterDamage() {

		yield return new WaitForSeconds (9.0f);
		waterDamage = false; 
		waterButton.GetComponent<Image> ().sprite = buttonOff;
		hose.enableHose (waterDamage);
	}
}
