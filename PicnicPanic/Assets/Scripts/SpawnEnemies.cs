﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour 
{

	public GameObject fireAntPrefab;

	public Vector3 SpawnLocation;
	public Vector3 spawnSide;

	public List<GameObject> spawnPointsList;
	int fireAntNumber;

	float spawnRate = 2f; 
	float lastSpawned = 0.0f;

	// Use this for initialization
	void Start () 
	{
		fireAntNumber = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{	
		if (spawnRate < 0) {
			spawnRate = 0; 
		} else {
			spawnRate = spawnRate;
		}

		lastSpawned += Time.deltaTime;
		if(lastSpawned >= spawnRate) {
				ChooseSpawnPoint ();
			 	GameObject fireAntGameObject;
				fireAntGameObject = Instantiate (fireAntPrefab, SpawnLocation, Quaternion.identity);
			fireAntNumber++;
			fireAntGameObject.name = "FireAnt: " + fireAntNumber.ToString();
			lastSpawned = 0;
		}


	}

	void ChooseSpawnPoint()
	{
		//SpawnLocation = Random.Range( 0, spawnPointsList.Count );

		int value = Random.Range (0, 4);
		/* 
		 * 0 - top
		 * 1 - right
		 * 2 - bottom
		 * 3 - left
 		*/
		if (value == 0) 
		{
			SpawnLocation = new Vector3 (Random.Range (-15, 16), 11, 0);
		} 
		else if (value == 1) 
		{
			SpawnLocation = new Vector3 (20, Random.Range(-8,8), 0);
		}

		else if (value == 2) 
		{
			SpawnLocation = new Vector3 (Random.Range (-15, 16), -11, 0);
		}

		else if (value == 3) 
		{
			SpawnLocation = new Vector3 (- 20, Random.Range(-8,8), 0);
		}

	}


}
