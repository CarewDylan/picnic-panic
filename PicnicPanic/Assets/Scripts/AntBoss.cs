﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntBoss : MonoBehaviour {


	public int health = 100;
	public bool isFighting = false;
	Vector3 idlePosition;
	Vector3 fightPosition;
	public static AntBoss boss; 
	// Use this for initialization
	void Start () {
		idlePosition = new Vector3 (-2.04f, -30f, 2f);
		fightPosition = new Vector3 (-2.04f, 2f, 2f);
		//gameObject.transform.position = idlePosition;
		isFighting = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isFighting && TouchControl.controlManager.score % 10 == 0 && TouchControl.controlManager.score > 0) {
			isFighting = true; 
			//gameObject.transform.position = Vector3.Lerp (transform.position, fightPosition, 10.0f * Time.deltaTime);
			//gameObject.transform.position = fightPosition;
		}

		if (isFighting) {
			gameObject.transform.position = Vector3.Lerp (transform.position, fightPosition, 10.0f * Time.deltaTime);
		} else {
			gameObject.transform.position = idlePosition;
		}


		if (health <= 0) {
			//transform.Translate (idlePosition);
			health = 100;
			TouchControl.controlManager.score++;
			gameObject.transform.position = Vector3.Lerp (transform.position, idlePosition, 10.0f * Time.deltaTime);
			isFighting = false; 
		}
	}

	public void DecreaseHeatlh () {
		health -= 100;
	}

	public void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "FireAnt") {
			Physics2D.IgnoreCollision (other.collider, GetComponent<Collider2D>());
		}
	}
}
