﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntScript : MonoBehaviour {

	public Rigidbody2D left_link; //top most links
	public Rigidbody2D right_link; //top most links
	public AntBoss antboss; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (antboss.isFighting) {
			left_link.AddForce (new Vector2 (Random.Range(-400,350), 80));
			right_link.AddForce (new Vector2 (Random.Range(-400,350), 80));
		}

	}

	public void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "AntLink" || other.collider.tag == "FireAnt") {
			Physics2D.IgnoreCollision (other.collider, GetComponent<Collider2D>());
		} 
	}
}
