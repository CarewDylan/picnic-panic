﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Food : MonoBehaviour 
{
	public FireAnt fireAntScript;

	public List <GameObject> fireAntScriptList;

	public Slider foodSlider;


	public float currentHealth;

	public float fireAntAttack;


	public bool isColliding; 
	// Use this for initialization
	void Start () 
	{
		isColliding = false;
		currentHealth = 100;
		fireAntAttack = fireAntScript.FA_Attack;

	}
	
	// Update is called once per frame
	void Update () 
	{
		foodSlider.value = currentHealth;

	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "FireAnt") {
			//if this ant's current food target does not equal me, dont add it to the list
			if (other.gameObject.GetComponent<FireAnt> ().currentFoodTarget == this.gameObject) {
				fireAntScriptList.Add (other.gameObject);
			}
		} 
//		if (other.gameObject.tag == "AntLink" || other.gameObject.tag == "AntBoss") {
//
//			Debug.Log ("DOWNDOWNDOWNDOWNv ");
//			CollideDecreaseHealth (); 
//			Physics2D.IgnoreCollision (other.collider, GetComponent<Collider2D> ());
//		}


	}

	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.tag == "FireAnt") {
			//if this ant's current food target does not equal me, dont damage me
			if (other.gameObject.GetComponent<FireAnt> ().currentFoodTarget == this.gameObject) {
				currentHealth -= fireAntAttack * fireAntScriptList.Count * Time.deltaTime;
			} 
		} 
		foodSlider.value = currentHealth;
		if (currentHealth <= 0) {
			//other.gameObject.SendMessage ("RemoveFoodFromList", );
			if (other.gameObject.tag == "FireAnt") {
				other.gameObject.SendMessage ("ChooseNewFoodTarget");
			}

			foodSlider.gameObject.SetActive (false);
			Destroy (this.gameObject);
		}
			
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.tag == "FireAnt" && other.gameObject.GetComponent<FireAnt> ().currentFoodTarget == this.gameObject) 
		{
			fireAntScriptList.Remove (other.gameObject);
		}
	}

	public void CollideDecreaseHealth () {
		currentHealth -= 0.25f;
	} 
}
