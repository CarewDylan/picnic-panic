﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rain : MonoBehaviour {

	public Rigidbody2D rb2d;
	public Hose hose; 

	private float RotateSpeed = 3f;
	private float Radius = 2f;
	Vector2 currentPos;
	private Vector2 centre;
	private float angle;
	void Start () {
	}

	// Update is called once per frame
	void Update () {

		if (!TouchControl.controlManager.waterDamage) {
			Destroy (gameObject);
		}

		for (int i = 0; i < Input.touchCount; i++) {
			Vector3 posFromScreen = Camera.main.ScreenToWorldPoint (Input.GetTouch (i).position); 
			currentPos = new Vector3 (posFromScreen.x, posFromScreen.y, 10);
			gameObject.transform.position = currentPos;
			centre = currentPos; 
		
		}
	
		angle += RotateSpeed * Time.deltaTime;

		var offset = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * Radius;
		transform.position = centre + offset;
	
	
	}
		
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "FireAnt") {
			FireAnt ant = (FireAnt)(((GameObject)other.gameObject).GetComponent<FireAnt>());
			ant.decreaseHealth();
			//Destroy (gameObject);
		}
	}


}
