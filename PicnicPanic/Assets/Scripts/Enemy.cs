﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	int health;

	// Use this for initialization
	void Start () {

		health = 100;	
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			Destroy (gameObject);
		}
	}

	public void decreaseHealth () {
		health -= 50;
		Debug.Log("health decrease!!@!@");

		return;
	}
}
