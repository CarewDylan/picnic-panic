﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAnt : MonoBehaviour
{
	public float FA_Health;
	public float FA_Attack;
	public float FA_MoveSpeed;
	int health = 200;
	Rigidbody2D FA_Rigidbody2D;

	public List<GameObject> foodObjectsList;

	GameObject food1;
	GameObject food2;
	GameObject food3;
	GameObject food4;
	GameObject food5;
	GameObject food6;

	public GameObject currentFoodTarget;

	void Awake()
	{
		food1 = GameObject.Find ("Food1");
		foodObjectsList.Add (food1);

		food2 = GameObject.Find ("Food2");
		foodObjectsList.Add (food2);

		food3 = GameObject.Find ("Food3");
		foodObjectsList.Add (food3);

		food4 = GameObject.Find ("Food4");
		foodObjectsList.Add (food4);

		food5 = GameObject.Find ("Food5");
		foodObjectsList.Add (food5);

		food6 = GameObject.Find ("Food6");
		foodObjectsList.Add (food6);
	}

	void Start()
	{
		FA_Rigidbody2D = GetComponent<Rigidbody2D> ();
		GetFoodTarget ();
	}

	void FixedUpdate()
	{
		Movement ();

		if (health <= 0) {
			//Destroy (gameObject);
			//TouchControl.controlManager.score++;
		
		}
		if (TouchControl.controlManager.gameOver) {
			Destroy (gameObject);
		}
	}

	void Movement()
	{
		//put movement code here
		//What is the difference in position?
		if (currentFoodTarget == null) 
		{
			ChooseNewFoodTarget ();	
		}

		if (currentFoodTarget != null) 
		{
			Vector3 diff = (currentFoodTarget.transform.position - transform.position);

			//We use aTan2 since it handles negative numbers and division by zero errors. 
			float angle = Mathf.Atan2(diff.y, diff.x);

			//Now we set our new rotation. 
			transform.rotation = Quaternion.Euler(0f, 0f, angle * Mathf.Rad2Deg + -90.0f);


			Vector2 velocity = new Vector2((transform.position.x - currentFoodTarget.transform.position.x) * FA_MoveSpeed, 
				(transform.position.y - currentFoodTarget.transform.position.y) * FA_MoveSpeed);
			FA_Rigidbody2D.velocity = -velocity;	
		}

	}
	
	void GetFoodTarget()
	{
		currentFoodTarget = foodObjectsList [Random.Range (0, foodObjectsList.Count)];
	}

	void ChooseNewFoodTarget()
	{
		foodObjectsList.Remove(currentFoodTarget);
		if (foodObjectsList.Count > 0) 
		{
			currentFoodTarget = foodObjectsList [Random.Range (0, foodObjectsList.Count)];
		}
		if (foodObjectsList.Count <= 0) 
		{
			//END GAME
			TouchControl.controlManager.gameOver = true;
		}
	}

	public void decreaseHealth () {
		health -= 100; 
		TouchControl.controlManager.score++;
		Destroy (gameObject);
		return;
	}

	public void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "FireAnt" || other.collider.tag == "AntBoss") {
			Physics2D.IgnoreCollision (other.collider, GetComponent<Collider2D>());
		}
	}
}
