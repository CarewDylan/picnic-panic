﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchCount : MonoBehaviour {
	private Text touchCount;
	// Use this for initialization
	void Start () {
		touchCount = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		touchCount.text = Input.touchCount.ToString ();
	}
}
