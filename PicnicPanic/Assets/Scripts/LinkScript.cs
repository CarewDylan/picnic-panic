﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkScript : MonoBehaviour {

 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "AntLink" || other.tag == "FireAnt") {
			Physics2D.IgnoreCollision (other, GetComponent<Collider2D>());
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		if (other.tag == "Food") {
			Food currentFood = (Food)(((GameObject)other.gameObject).GetComponent<Food> ());
			//Physics2D.IgnoreCollision (other.collider, GetComponent<Collider2D> (), true);

			currentFood.CollideDecreaseHealth ();
		}
	}
}
