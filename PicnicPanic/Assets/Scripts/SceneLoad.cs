﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour {
	public Text loadText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0 || Input.GetMouseButtonDown(0)) {
			loadText.gameObject.SetActive (false);
			SceneManager.LoadScene ("Gameplay", LoadSceneMode.Additive);
		}
	}
}
